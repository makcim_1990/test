<?php
/*
 * Class for work with Database
 */

include_once('../config/db.php');

class filmsModel
{
    private $_db;
    private $_isActive = 1;

    public function __construct()
    {
        $config = new MyConf;
        $this->_db = mysqli_connect($config->dblocation, $config->dbuser, $config->dbpasswd, $config->dbname);
        if (!$this->_db) {
            die("Error accessing MySql");
        }
    }

    /**
     * Insert data to a table
     * @param $data
     * @return bool|mysqli_result
     */
    public function add($data)
    {
        return mysqli_query($this->_db, "INSERT INTO `films` VALUES ( NULL, '".$data[name]."', '".$data[year]."', '".$data[isActive]."');");
    }

    /**
     * Sample data from the table
     * @return bool|mysqli_result
     */
    public function review(){
        $query = mysqli_query($this->_db, "SELECT `name`, `year` FROM `films` WHERE `isActive`= '" . $this->_isActive ."' ORDER BY `id`;");

        return $query;

    }
}
