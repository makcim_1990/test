<?php
/*
 * Controller class
 */
include_once '../models/FilmsModel.php';

class filmController
{
    /**
     * @param $data
     * @return bool|mysqli_result
     */
    public function add($data)
    {
        return (new filmsModel)->add($data);
    }

    /**
     * @return bool|mysqli_result
     */
    public function review()
    {
        $model = new filmsModel();
        $rsData = $model->review();
        return $rsData;
    }
}