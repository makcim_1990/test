<a class="btn btn-default" href="/new.php" role="button">Добавить новый фильм</a>
<table class="table table-striped">
    <tr>
        <th>Id</th>
        <th>Имя</th>
        <th>Год</th>
    </tr>
    <?php include_once '../controllers/FilmController.php';
    $iteration = 1;
    $data = (new filmController())->review();
    foreach ($data as $item) {?>
        <tr>
            <td><?php echo $iteration?></td>
            <td><?php echo $item['name']?></td>
            <td><?php echo $item['year']?></td>
        </tr>
        <?php $iteration++;
    }?>
</table>