<div class="container">
    <div class="row">
        <div class="col-md-5">
            <form id="add">
                <div class="form-group">
                    <input type="text" class="form-control" id="film_name" name="name" placeholder=" |Название фильма"
                           required/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="film_year" name="year" placeholder=" |Год" required/>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="isActive" id="optionsRadios1" value="0">Не отображать
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="isActive" id="optionsRadios1" value="1" checked>Отображать
                    </label>
                </div>
                <input type="submit" class="btn btn-success" id="send" name="send" value="Добавить"/>
            </form>
            <a class="btn btn-default" href="/index.php" role="button">Просмотреть весь список фильмов</a>
        </div>
        <div class="col-md-7">
        </div>
    </div>
</div>
<script src="./js/main.js"></script>