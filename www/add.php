<?php
include_once '../controllers/FilmController.php';

$data = [];
foreach($_POST as $key => $value){
    $data[$key] = trim(stripslashes(strip_tags(htmlspecialchars($value))));
}

echo json_encode(['result' => (new filmController())->add($data)]);