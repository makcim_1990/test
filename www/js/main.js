var Main = Main || {};
Main.add =  $("#add");
Main.send = $("#send");

var firstfilm = 1896; //first film year
var curryear = 2016; //on current date

$("document").ready(function(){

    /**
     * Event button registration click
     */
    Main.send.click(function(e){
        e.preventDefault();
        var postdata = Main.add.serializeArray();

        if(Main.add.find('#film_name').val() == '') {
            alert('Введите название фильма');
            return;
        }

        if(Main.add.find('#film_year').val() == '') {
            alert('Введите год выхода фильма');
            return;
        }

        if(Main.add.find('#film_year').val() < firstfilm || Main.add.find('#film_year').val() > curryear) {
            alert('Введите корректный год выхода фильма');
            return;
        }

        $.ajax({
            url: "add.php",
            type: 'POST',
            dataType: 'json',
            data: postdata,
            success: function (data) {
                if(data.result == true){
                    alert('Добавлено успешно');
                } else {
                    alert('Ошибка добавления');
                }
            },
            error: function () {
                alert('error');
            }
        });
    });
});
